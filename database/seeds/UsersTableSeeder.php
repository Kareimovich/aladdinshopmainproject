<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->email = 'user@example.com';
        $user->password = bcrypt('password'); // password
        $user->save();


        $user_store = new User();
        $user_store->email = 'store@example.com';
        $user_store->password = bcrypt('password'); // password
        $user_store->save();


        $user_megastore = new User();
        $user_megastore->email = 'megastore@example.com';
        $user_megastore->password = bcrypt('password'); // password
        $user_megastore->save();


        $admin = new User();
        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('password'); // password
        $admin->save();

    }
}
