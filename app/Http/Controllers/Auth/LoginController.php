<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    public function handleProviderCallback($service)
    {
        $user = Socialite::driver($service)->user();
        $FindUser = User::where('email',$user->getEmail())->first();
        if($FindUser){
            Auth::login($FindUser);
        } else{

            $url  = str_replace(['?', 'v=4'], '', $user->getAvatar());
            $file = file_get_contents($url);
            $name = substr($user->name, 0, 3) . strrpos($user->getAvatar(), '/').rand(1, 170);
            \Storage::put('public/users/' . $name . '.png', $file, 'public');

            $NewUser = new User;
             $NewUser->email = $user->getEmail();
            if($user->getName()  == null){
                $userName = $user->getNickname();
            }
                  else{
                $userName = $user->getName();

                  }
                  $NewUser->name  = $userName;
                $NewUser->avatar  = 'users/' . $name.'.png';
                $NewUser->login_type  = ucwords($service);
                $NewUser->password  = bcrypt(123456);
                $NewUser->save();
                 Auth::login($NewUser);
            }
        return redirect('/');
    }
}
